# Zadanie świąteczne
#
# 1. Utwórz klasę elf z atrybutami
# name - typu str
# level - typu int
# class Elf:
#     def __init__(self, name, level):
#         self.name = name
#         self.level = level
#
#     def __str__(self):
#         return self.name + ";" + str(self.level)
#
# 2. Utwórz klasę gift
# name - typu str
# level_of_difficulty - typu int
# 3. Utwórz menu

# 1. Dodaj Elfa
# 2. Wyświetl Elfy
# 3. Wyprodukuj prezent
# 4. Lista wytworzonych prezentów
#
# Podczas dodawanie elfa, zapisze się w pliku elfs.txt jako linia.
# Przykład:
# lucek;1
#
# lucek - oznacza imie elfa
# 1 - oznacza level
# ; - oznacza separator ("odziela elementy name od level")
#
# Podczas produkcji prezentów, użytkownik zostanie zapytany o:
# nazwe prezentu
# poziom trudności wytworzenia prezentu
#
# Jeśli poziom trudności wytorzenia prezentu jest większy niż suma poziomów wszystkich elfów, to nie utworzy się. W przeciwnym razie prezent zostanie utworzony i zapisze sie do gifts.txt

# Zadanie świąteczne V2.0 --------------------------------------------------------

# Utwórz klasę: Elf z atrybutami
# id
# name
# level
#
# Utwórz klasę: Gift z atrybutami
# name
# difficulty
#
# Utwórz zmienną globalną Integer christmasCoin
#
# Utwórz Menu
#
# 1. Dodaj Elfa
# 2. Wyświetl Elfy
# 3. Ulepsz elfa
# 4. Wyprodukuj prezent
# 5. Lista wytworzonych prezentów
#
# Dodawanie Elfa
# Podczas dodawania elfa użytkownik zostanie zapytany o imie elfa.
# Domyślnie elf tworzy się z poziomem 1, id jest generowane (uuid).
#
# Wyświetlanie Eflów
# Wyświetla wszystkie elfy wraz z ich wszystkimi cechami
#
# Ulepszanie Elfa
# Ta opcja umożliwia ulepszenie elfa o 1 poziom.
# Ulepszenie kosztuje
# 1 - 4lvl   10 ChristmasCoinów
# 5 - 10lvl  15 ChristmasCoinów
#
# Wyprodukuj prezent
# Użytkownik zostanie poproszony o nazwe prezentu, poziom trudności wytworzenia przedmiotu.
# Jeśli poziom trudności wytworzenia prezentu jest większa niż suma leveli elfów, to wtedy
# prezent nie zostanie utworzony.
# Jeśli jednak elfy poziadają większą sume leveli, to prezent zostanie utworzony i
# przybędzie gift dificulty * 3 ChristmasCoinów.
#
# Lista wyprodukowanych prezentów
# Zwróci liste wyprodukowanych prezentów
#
# Dodatek do zadania
#
# Bonusowe Poziomy elfa
# Podczas ulepszania elfa wylosuj liczbe od 0 do 10. Jeśli ta liczba jest równa 7 to ulepsz elfa o 3 poziomy.
#
# Bonusowe punkty
# Podczas tworzenia prezentu wylosuj liczbe od 0 do 30, jeśli wylosowana liczba to 7, to pomnóż sume siły elfów o 3
#
# Elf złośnik
# Podczas tworzenia elfa wylosuj liczbe od 0 do 4, jeśli wylosowana liczba to 3, to elf utworzy się z levelem -3.

import elfs
import gifts
import uuid
import random

christmas_coin = 1000

def add_elf():
    num = random.randint(0,4)
    if(num == 3):
        poziom = "-3"
        print("Ups! To bedzie Elf Zlosnik")
    else:
        poziom = "1"

    identyfikator = str(uuid.uuid4())
    elfs.name = input("Podaj imię Elfa:\n")
    file = open("elfs.txt", "a")
    file.write(identyfikator + ";" + elfs.name + ";" + poziom + "\n")
    print("Brawo utworzyles elfa o imieniu " + elfs.name + " i poziomie = " + poziom)
    file.close()
    menu()

def show_elfs():
    try:
        file = open("elfs.txt", "r")
        for line in file:
            print(line.strip())
        file.close()
    except(Exception):
        not_found_elf()
    menu()

def not_found_elf():
    x = int(input("Chcesz utowrzyć nowy plik?\n 1. Tak 2. Nie\n"))
    if (x == 1):
        file = open("elfs.txt", "w")
        file.close()
    elif (x == 2):
        menu()
    else:
        print("Wybrano nieprawidlowa opcje")

def upgrade_elf():
    # global christmas_coin
    file = open("elfs.txt", "r")
    imie = input("Podaj Imie Elfa, ktorego chcesz ulepszyc: ")
    for line in file:
        x = line.strip()
        name = str(x.split(";", 2)[-2])
        level = int(x.split(";", 2)[-1])
        if imie == name:
            level += 1
            with open("elfs.txt", "w") as f:
                f.write(str(level))
            print("\nBrawo, poziom Elfa " + name + " wynosi teraz " + level)
            file.close()
            menu()

        # if elfs.level < 5:
        #     christmas_coin -= 10
        # else:
        #     christmas_coin -= 15
        
        #num = random.randint(0, 10)
        #if(num == 7):
             #elfs.level *= 3
             #print("\nTo Twoj szczesliwy dzien!)
        else:
            print("Nie ma takiego Elfa! Jesli chcesz dodac wybierz 1")
    file.close()
    menu()
    
def create_gifts():
    global christmas_coin
    suma_leveli = 0
    file = open("elfs.txt", "r")
    for line in file:
        x = line.strip()
        y = x.split(";", 2)[-2]
        suma_leveli += (int(y))
    file.close()

    gifts.name = input("Podaj nazwe prezentu: ")
    gifts.level = int(input("Podaj poziom trudnosci wytworzenia prezentu: "))
    if gifts.level > suma_leveli:
         print("Elfy sa zbyt slabe by wytworzyc prezent. Sprobuj ulepszyc Elfy.")
    else:
         file = open("gifts_list.txt", "a")
         file.write(gifts.name + ";" + str(gifts.level) + "\n")
         file.close()
         bonus = gifts.level * 3
         christmas_coin += bonus

         num = random.randint(0, 30)
         if(num == 7):
             suma_leveli = suma_leveli * 3
             print("\nTo Twoj szczesliwy dzien, suma leveli elfow wynosi teraz " + str(suma_leveli))

         print("\nBrawo, Elfy wyprodukja prezent o nazwie " + gifts.name)
         print("\nAktualnie suma Christmas coins wynosi: " + str(christmas_coin))
    menu()

def gifts_list():
    try:
        file = open("gifts_list.txt", "r")
        for line in file:
            x = line.strip()
            print(x.split(";", 1)[0])
        file.close()
    except(Exception):
        not_found_gifts()
    menu()

def not_found_gifts():
    x = int(input("Chcesz utowrzyć nowy plik?\n 1. Tak 2. Nie\n"))
    if (x == 1):
        file = open("gifts_list.txt", "w")
        file.close()
    elif (x == 2):
        menu()
    else:
        print("Wybrano nieprawidlowa opcje")

def menu():
    print("""
======ZADANIE ŚWIĄTECZNE=======

1. Dodaj Elfa
2. Wyświetl Elfy
3. Ulepsz Elfa
4. Wyprodukuj prezent
5. Lista wytworzonych prezentów
""")
    choise = int(input("Twój wybór: \n"))
    if (choise == 1):
        add_elf()
    if (choise == 2):
        show_elfs()
    if (choise == 3):
        upgrade_elf()
    if (choise == 4):
        create_gifts()
    if (choise == 5):
        gifts_list()
menu()